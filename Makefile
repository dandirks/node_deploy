test: coffee
	@./node_modules/.bin/mocha -u tdd -R spec --compilers coffee:coffee-script

coffee:
	@./node_modules/.bin/coffee --bare --compile --output lib/ src/

coffee_watch:
	@./node_modules/.bin/coffee --watch --bare --compile --output lib/ src/