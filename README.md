# Node Deploy

Node Deploy allows you to easily deploy new versions of code to production servers.

It is entirely flexible in that it allows you to define the actions to take when a new version of your application is sent to the server. It allows you to perform database queries to add tables, install new NPM modules, or perform any other action.

## Example

    :::coffeescript
    module.exports = deploy = new (require 'node_deploy')
    execFile = require('child_process').execFile

    deploy.before = ->
      execFile './stop-server.sh' #stop current application

    deploy.after = ->
      execFile './start-server.sh' #start current application

    deploy.beforeEach = ->
      @db = new db #create a new DB connection

    deploy.afterEach = ->
      @db.close() #close the DB connection

    deploy.version 'hypothetical-chipmunk-4', ->
      @db.query ... #update database with changes

    deploy.version 'hypothetical-chipmunk-5', ->
      email.user ... #email users about new features