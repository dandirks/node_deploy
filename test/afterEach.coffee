deploy = require '../lib/deploy'
_ = require 'underscore'
assert = require 'assert'

suite 'afterEach version', ->
  setup ->
    @deploy = new deploy

  test 'should allow scripts to be executed after versions', (done) ->
    @deploy.afterEach = ->
      assert(@deployed, 'afterEach was not called after deployment')
      done()

    @deploy.version 'theoretical-hedgehog-1', ->
      @deployed = true


    @deploy.execute('theoretical-hedgehog-1')

  test 'should have the current version being executed in context', (done) ->
    @deploy.afterEach = ->
      assert.equal(@version, 'theoretical-hedgehog-1', 'version not set in context')
      done()

    @deploy.version 'theoretical-hedgehog-1', ->

    @deploy.execute('theoretical-hedgehog-1')

  test 'should run after every script is executed', (done) ->
    done = _.after(2, done)
    @deploy.afterEach = ->
      assert(@deployed, 'afterEach was not called after deployment')
      done()

    @deploy.version 'theoretical-hedgehog-1', ->
      @deployed = true

    @deploy.version 'theoretical-hedgehog-1', ->
      @deployed = true

    @deploy.execute('theoretical-hedgehog-1')