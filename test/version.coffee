deploy = require '../lib/deploy'
_ = require 'underscore'
assert = require 'assert'

suite 'deploy versioning', ->
  setup ->
    @deploy = new deploy

  test 'should allow for version-based scripts to be executed', (done) ->
    @deploy.version 'hypothetical-chipmunk-1', ->
      assert(true, 'hypothetical-chipmunk-1 deployed!')
      done()

    @deploy.execute('hypothetical-chipmunk-1')

  test 'should allow for any arbitrary version name', (done) ->
    @deploy.version 'hasOwnProperty', ->
      assert(true, 'hasOwnProperty deployed!')
      done()

    @deploy.execute('hasOwnProperty')

  test 'should only execute the current version script', (done) ->
    @deploy.version 'hypothetical-chipmunk-1', ->
      assert(true, 'hypothetical-chipmunk-1 deployed!')
      done()

    @deploy.version 'hypothetical-chipmunk-2', ->
      throw new Error('script should not have been called!')
      done()

    @deploy.execute('hypothetical-chipmunk-1')

  test 'should allow you to have multiple scripts for the same version', (done) ->
    done = _.after(2, done)

    @deploy.version 'hypothetical-chipmunk-1', ->
      assert(true, 'hypothetical-chipmunk-1 deployed!')
      done()

    @deploy.version 'hypothetical-chipmunk-1', ->
      assert(true, 'hypothetical-chipmunk-1 deployed!')
      done()

    @deploy.execute('hypothetical-chipmunk-1')

  test 'should keep the same context and be called in order', (done) ->
    @deploy.version 'hypothetical-chipmunk-1', ->
      @called = true

    @deploy.version 'hypothetical-chipmunk-1', ->
      assert(@called, 'context not kept')
      done()

    @deploy.execute('hypothetical-chipmunk-1')

  test 'should do nothing if no versions match', (done) ->
    _.defer -> done()

    @deploy.version 'hypothetical-chipmunk-1', ->
      throw new Error('script should not have been called!')

    @deploy.execute('hypothetical-chipmunk-15')

  test 'should have the current version being executed in context', (done) ->
    @deploy.version 'hypothetical-chipmunk-1', ->
      assert.equal(@version, 'hypothetical-chipmunk-1', 'version not set in context')
      done()

    @deploy.execute('hypothetical-chipmunk-1')