deploy = require '../lib/deploy'
_ = require 'underscore'
assert = require 'assert'

suite 'beforeEach version', ->
  setup ->
    @deploy = new deploy

  test 'should allow scripts to be executed before versions', (done) ->
    @deploy.beforeEach = ->
      @before = true

    @deploy.version 'theoretical-hedgehog-1', ->
      assert(@before, 'beforeEach was not called')
      done()

    @deploy.execute('theoretical-hedgehog-1')

  test 'should have the current version being executed in context', (done) ->
    @deploy.beforeEach = ->
      assert.equal(@version, 'theoretical-hedgehog-1', 'version not set in context')
      done()

    @deploy.version 'theoretical-hedgehog-1', ->

    @deploy.execute('theoretical-hedgehog-1')

  test 'should run before every script is executed', (done) ->
    done = _.after(2, done)

    @deploy.beforeEach = ->
      @count ?= 0
      @count++

    @deploy.version 'theoretical-hedgehog-1', ->
      assert.equal(@count, 1, 'count was not correct (1)')
      done()

    @deploy.version 'theoretical-hedgehog-1', ->
      assert.equal(@count, 2, 'count was not correct (2)')
      done()

    @deploy.execute('theoretical-hedgehog-1')