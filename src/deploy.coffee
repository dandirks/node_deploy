class Deploy
  constructor: ->
    @versions = Object.create(null)
    @beforeEach = ->
    @afterEach = ->

  version: (name, script) =>
    @versions[name] ?= []
    @versions[name].push(script)

  execute: (version) =>
    return if not @versions[version]
    context = { version: version }
    for script in @versions[version]
      @beforeEach.call(context)
      script.call(context)
      @afterEach.call(context)
    return

module.exports = Deploy